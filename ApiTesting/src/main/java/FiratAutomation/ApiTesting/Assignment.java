package FiratAutomation.ApiTesting;

import java.util.Random;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Assignment {

	public String mobilenum;
	static Random rand = new Random();
	public static int number = rand.nextInt(900000)+100000;

	
	
    @Test(priority=1, description="type customer informations for registration")
	public void ApplyCredit() {
    	
    	String name = Name();
    	String lastname = LastName();
    	String birthdate = BirthDate();
    	String DNIValue = GenerateDNI();
    	mobilenum = MobileNum();
    	System.out.println(name + " " + lastname+" "+birthdate+" "+ DNIValue + " " + mobilenum);
    	
    	
		RestAssured.baseURI="https://test-v2-api.mykredit.com/api/customer/light";
		//Request object
		RequestSpecification httpRequest = RestAssured.given();
		
		
		JSONObject requestparam = new JSONObject();
		
	
		requestparam.put("acceptAgreement",true);
		requestparam.put("acceptPolicy",true);
		requestparam.put("birthdate", birthdate);
		requestparam.put("countryCode", "+34");
		requestparam.put("emailAddress", "blackkfredo2@gmail.com");
		requestparam.put("firstName",name);
		requestparam.put("gender", "male");
		requestparam.put("lastName", lastname);
		requestparam.put("mobilePhone", mobilenum);
		requestparam.put("nationalID", DNIValue);
		
		
		httpRequest.header("Content-Type","application/json; charset=utf-8");
		httpRequest.body(requestparam.toJSONString());
		Response response = httpRequest.post("/register");
		
		String responseBody = response.getBody().asString();	
		System.out.println(responseBody);
		
		int statusLine = response.getStatusCode();
		System.out.println("Status"+" "+statusLine);
		Assert.assertEquals(statusLine, 200);
		
		
		
		//{"birthDate":"1992-06-02","mobilePhone":"632137106","countryCode":"+34","firstName":"Ozan","lastName":"Fasil","nationalID":"05612313Z","emailAddress":"blackkfredo@gmail.com","gender":"male","acceptPolicy":true,"acceptAgreement":true}	
		
	}
    
   @Test(priority=2, description="type pin value for validation")
    public void PinValidate() {
    	
	   //RestAssured.baseURI="https://test-v2-api.mykredit.com/api/customer/light";
    	RestAssured.baseURI="https://test-v2-api.mykredit.com/api/membership";
    	RequestSpecification httpRequest2 = RestAssured.given();
    	JSONObject requestparam2 = new JSONObject();
    	System.out.println(mobilenum.substring(5)); 
    	
    	requestparam2.put("countryCode", "+34");
    	requestparam2.put("phoneNumber", mobilenum);
        requestparam2.put("pin",mobilenum.substring(5));
        
        
    	httpRequest2.header("Content-Type","application/json; charset=utf-8");
		httpRequest2.body(requestparam2.toJSONString());
		Response response = httpRequest2.request(Method.POST,"/login");
		//Response response = httpRequest2.request(Method.POST,"/pinValidate");
		String responseBody = response.getBody().asString();
		System.out.println(responseBody);
		
		int status = response.getStatusCode();
		System.out.println("Status"+" "+status);
		Assert.assertEquals(status, 200);
    	
    }
   
  @Test(priority=3, description="type adress informations")
   public void InputAdress() {
   	
   	
   	RestAssured.baseURI="https://test-v2-api.mykredit.com/api/customer/light";
   	RequestSpecification httpRequest3 = RestAssured.given();
   	JSONObject requestparam3 = new JSONObject();
     
       requestparam3.put("addressType","home");
       requestparam3.put("companyName","Wipro");
       requestparam3.put("country","Spain");
       requestparam3.put("county","Madrid");
       requestparam3.put("details","Madrid Sokak");
       requestparam3.put("district","");
       requestparam3.put("flatNumber","2A");
       requestparam3.put("houseNumber","45");
       requestparam3.put("postCode","28192");
       requestparam3.put("street","ROAD");
       requestparam3.put("town","Madrid");

       
       
   	    httpRequest3.header("Content-Type","application/json; charset=utf-8	");
		httpRequest3.body(requestparam3.toJSONString());
		Response response = httpRequest3.request(Method.PUT,"/saveAddress");
		String responseBody = response.getBody().asString();
		System.out.println(responseBody);
		
		int status = response.getStatusCode();
		System.out.println("Status"+" "+status);
		Assert.assertEquals(status, 200);
   	
   }
    
    
	 public static String Name () {
		 
			String str = String.valueOf(number);
	        String otherString = "Name";
	        otherString=otherString+str;
	        return otherString;
  }
	 public static String LastName () {
		
			String str = String.valueOf(number);
	        String otherString = "LastName";
	        otherString=otherString+str;
	        return otherString;
}
	 public static String BirthDate() {
		 
		 int randomdate = (int) (Math.random() * (2000 - 1940) + 1940); 
		 String str = String.valueOf(randomdate);
		 int randommonth = (int) (Math.random() * (12 - 1) + 1); 
		 String str2 = String.valueOf(randommonth);
		 int randomday = (int) (Math.random() * (30 - 1) + 1); 
		 String str3 = String.valueOf(randomday);
		 String dateinfo = str+"-"+str2+"-"+str3;
		 return dateinfo;
		 
	 }
		public static String GenerateDNI() {
			
			Random rand = new Random();
			int dni = rand.nextInt(90000000)+100000;
			String letters = "TRWAGMYFPDXBNJZSQVHLCKE";
			char let = letters.charAt(dni % letters.length());
			String dnivalue = "" + dni + let;
			return dnivalue;

		}
		
		public static String MobileNum() {
			
			Random rand = new Random();
			int number = 10000000 + rand. nextInt(90000000);
			String str = String.valueOf(number);
			String firstdigit = "6";
			firstdigit=firstdigit+str;
	        return firstdigit;

		}

}
